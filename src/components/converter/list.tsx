import { ReactElement } from "react";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import { makeStyles } from "@material-ui/core/styles";
import { CurrencyService, IRatesMap } from "../../services/currency/CurrencyService";

const REQUIRED_CCYS = ["EUR", "RUB", "AUD", "GBP", "CAD", "USD"];

const useStyles = makeStyles(() => ({
  item: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    fontSize: 11,
    fontFamily: "Inter",
    fontWeight: 400,
    lineHeight: "16px",

    "&:hover": {
      background: "rgba(4, 197, 58, 0.2);",
      cursor: "pointer",
    },
  },
  ccy: {
    fontWeight: 600,
  },
}));

interface Props {
  selectedCCY: string;
  rates: IRatesMap;
  amount: number;
}

export default function RateList({
  selectedCCY,
  rates,
  amount,
}: Props): ReactElement {
  const classes = useStyles();
  const items = Object.entries(rates).filter(([ccy]) => {
    if (ccy === selectedCCY) return false;
    return REQUIRED_CCYS.includes(ccy);
  });

  return (
    <List data-testid="c-converter-list">
      {items.map(([ccy]) => {
        return (
          <ListItem key={ccy} className={classes.item}>
            <span className={classes.ccy}>{ccy}</span>
            <span>
              {(
                CurrencyService.getRateForCCYPair(selectedCCY, ccy, rates) *
                amount
              ).toFixed(2)}
            </span>
          </ListItem>
        );
      })}
    </List>
  );
}
