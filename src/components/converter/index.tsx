import { ReactElement, useState, useEffect, useRef } from "react";
import Paper from "@material-ui/core/Paper";
import Box from "@material-ui/core/Box";
import Select from "./select";
import Input from "./input";
import RateList from "./list";
import { useGetCurrencyRates } from "../../hooks/getCurrencyRates";
import CurrencyService, {
  CurrencyService as CCYService,
} from "../../services/currency/CurrencyService";
import useStyles from "./styles";

interface Props {
  title: string;
}

export default function Converter({ title }: Props): ReactElement {
  const boxEl = useRef(null);
  const { data, mutate } = useGetCurrencyRates();
  const [amountInput, setAmount] = useState(
    String(CurrencyService.getStored().amount)
  );
  const [selected, setSelected] = useState(
    CurrencyService.getStored().currency
  );

  const getAmountFromInput = (input: string): number => {
    const number = parseFloat(input);
    if (Number.isNaN(number)) return 1;
    return number;
  };

  const amount = getAmountFromInput(amountInput);

  useEffect(() => {
    const amount = getAmountFromInput(amountInput);
    CurrencyService.store(amount, selected);
  }, [amountInput, selected]);

  useEffect(() => {
    mutate();
  }, [selected, mutate]);

  const handleSelect = (e: React.ChangeEvent<{ value: unknown }>) =>
    setSelected(e.target.value as string);

  const handleAmountChange = (e: React.ChangeEvent<{ value: unknown }>) => {
    const number = parseFloat(e.target.value as string);
    if (Number.isNaN(number)) return setAmount("");
    setAmount(String(number));
  };

  const rates = (data && CCYService.normalizeData(data)) || {};

  const classes = useStyles();
  return (
    <div data-testid="c-converter">
      <Paper className={classes.root}>
        <div className={classes.header}>
          <span>
            <h4>{title}</h4>
          </span>
        </div>
        <Box component="div" px={1.5} mt={2} mx={0.5} position="relative">
          <div ref={boxEl}>
            <Select
              handleChange={handleSelect}
              selected={selected}
              anchorEl={boxEl.current}
              currencies={rates}
            />
          </div>
        </Box>
        <Box m={2}>
          <Input value={amountInput} onChange={handleAmountChange} />
        </Box>
        <Box>
          <RateList amount={amount} selectedCCY={selected} rates={rates} />
        </Box>
      </Paper>
    </div>
  );
}
