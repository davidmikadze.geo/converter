import {
  prettyDOM,
  render,
  findByText,
  getByRole,
} from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import UserEvent from "@testing-library/user-event";
import CCYService from "../../services/currency/CurrencyService";
import Converter from "./";

let initialData: { amount: number; currency: string };

beforeAll(() => {
  initialData = CCYService.getStored();
});

test("should populate default amount and curreny on first render", () => {
  const { getByTestId } = render(<Converter title="Converter" />);
  const select = getByTestId("c-converter-select-input");
  const input = getByTestId("c-converter-amount-input");
  const CCY = initialData?.currency;
  const amount = initialData?.amount;

  expect(select).toHaveValue(CCY);
  expect(input.querySelector("input")).toHaveValue(String(amount));
});

test("should update and show correct rates on currency change", async () => {
  const { getByTestId, getByRole } = render(<Converter title="Converter" />);
  const select = getByTestId("c-converter-select");

  const button = getByRole("button");

  UserEvent.click(button);

  const BGN = await findByText(document.body, "BGN");

  userEvent.click(BGN);
  expect(select).toHaveTextContent("BGN");
});

test("should persist last used currency and number", async () => {
  const { getByTestId } = render(<Converter title="Converter" />);
  const select = getByTestId("c-converter-select");
  const input = getByTestId("c-converter-amount-input").querySelector("input");
  const initialAmount = input?.value;

  const button = getByRole(select, "button");

  UserEvent.click(button);

  const PLN = await findByText(document.body, "PLN");

  userEvent.click(PLN);
  input && userEvent.type(input, "100");

  const stored = CCYService.getStored();

  expect(stored.amount === Number(initialAmount + "100"));
  expect(stored.currency === "PLN");
});
