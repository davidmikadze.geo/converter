import { ReactElement } from "react";
import MenuItem from "@material-ui/core/MenuItem";
import ListItemText from "@material-ui/core/ListItemText";
import KeyboardArrowDownIcon from "@material-ui/icons/KeyboardArrowDown";
import { makeStyles } from "@material-ui/core/styles";
import { TextField } from "@material-ui/core";
import { IRatesMap } from "../../services/currency/CurrencyService";

const useStyles = makeStyles(() => ({
  selectInput: {
    width: "100%",
    borderRadius: 2,
    boxSizing: "border-box",
    height: 32,

    "& .Mui-focused .MuiOutlinedInput-notchedOutline": {
      border: "1px solid rgba(0, 0, 0, 0.1)",
      borderRadius: 2,
      background: "transparent",
    },

    "& .MuiListItemText-root": {
      margin: 0,
    },
  },
  select: {
    padding: 8,
    borderRadius: 2,
    background: "transparent",

    "&:focus": {
      background: "transparent",
    },

    "&:hover": {
      borderRadius: 2,
    },
  },
  list: {
    width: "100%",
    color: "#fff",
    position: "relative",
    backgroundColor: "#222222",
    overflow: "auto",
    maxHeight: 300,
  },
  menuPaper: {
    backgroundColor: "#222222",
    outline: 0,
  },
  listItem: {
    padding: 0,
    paddingRight: 16,
    paddingLeft: 16,
    minHeight: 0,

    "&:hover": {
      background: "rgba(4, 197, 58, 0.2)",
    },

    "&.Mui-selected": {
      background: "#04C53A",

      "&:hover": {
        background: "#04C53A",
      },
    },
  },
  listItemText: {
    fontSize: 12,
    fontWeight: 400,
    lineHeight: "16px",
    fontFamily: "Inter",
  },
  icon: {
    fontSize: 16,
    top: "calc(50% - 8px)",
  },
}));

interface Props {
  currencies: IRatesMap;
  anchorEl: Element | null;
  selected: string;
  handleChange: React.ChangeEventHandler<{ value: unknown }>;
}

export default function SelectCCY({
  currencies,
  anchorEl,
  selected,
  handleChange,
}: Props): ReactElement {
  const classes = useStyles();
  const popoverWidth = anchorEl?.getBoundingClientRect().width;

  return (
    <TextField
      label={false}
      variant="outlined"
      select
      value={selected}
      onChange={handleChange}
      data-testid="c-converter-select"
      className={classes.selectInput}
      inputProps={{
        "data-testid": "c-converter-select-input",
      }}
      InputProps={{
        classes: {
          root: classes.selectInput,
        },
      }}
      SelectProps={{
        className: classes.selectInput,
        variant: "outlined",
        classes: {
          icon: classes.icon,
          outlined: classes.select,
        },
        IconComponent: KeyboardArrowDownIcon,
        MenuProps: {
          classes: {
            paper: classes.menuPaper,
            list: classes.list,
          },
          anchorOrigin: {
            vertical: "top",
            horizontal: "left",
          },
          anchorEl,
          getContentAnchorEl: null,
        },
      }}
    >
      {Object.keys(currencies).map((key) => (
        <MenuItem
          className={classes.listItem}
          style={{ width: popoverWidth }}
          value={key}
          key={`item-${key}`}
        >
          <ListItemText
            primary={key}
            primaryTypographyProps={{ className: classes.listItemText }}
          />
        </MenuItem>
      ))}
    </TextField>
  );
}
