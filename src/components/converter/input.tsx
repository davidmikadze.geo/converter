import { ReactElement } from "react";
import TextField from "@material-ui/core/TextField";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(() => ({
  input: {
    width: "100%",
    borderRadius: 2,
    background: "transparent",
    boxSizing: "border-box",
    height: 32,

    "& .MuiOutlinedInput-notchedOutline": {
      border: "1px solid rgba(0, 0, 0, 0.1)",
    },

    "&:hover .MuiOutlinedInput-notchedOutline": {
      border: "1px solid rgba(0, 0, 0, 0.5)",
    },

    "& .Mui-focused .MuiOutlinedInput-notchedOutline": {
      border: "2px solid #04C53A",
      borderRadius: 2,
      background: "transparent",

      "&:hover": {
        border: "none",
      },
    },
  },
  outer: {
    borderRadius: 2,
  },
  inner: {
    padding: 8,
    border: "1px solid rgba(0, 0, 0, 0.1)",
    borderRadius: 2,
    fontSize: 12,
    fontWeight: 400,
    lineHeight: "16px",
    fontFamily: "Inter",
  },
}));

interface Props {
  value: string;
  onChange: React.ChangeEventHandler<{ value: unknown }>;
}

export default function Input({ value, onChange }: Props): ReactElement {
  const classes = useStyles();
  return (
    <TextField
      className={classes.input}
      data-testid="c-converter-amount-input"
      onChange={onChange}
      value={value}
      variant="outlined"
      InputProps={{
        className: classes.outer,
        classes: { input: classes.inner },
      }}
    />
  );
}
