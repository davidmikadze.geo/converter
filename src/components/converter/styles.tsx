import { makeStyles } from "@material-ui/core/styles";

export default makeStyles(() => ({
  root: {
    width: 220,
    background: "#FFFFFF",
    border: "0.5px solid rgba(0, 0, 0, 0.2)",
    borderRadius: "2px",
  },
  header: {
    display: "flex",
    alignItems: "center",
    height: 48,
    padding: 8,
    fontFamily: "Inter",
    boxSizing: "border-box",
    borderBottom: "1px solid #E5E5E5",

    "& span": {
      display: "flex",
      alignItems: "center",
      padding: 8,

      "& h4": {
        fontWeight: 600,
        fontSize: 11,
        lineHeight: "16px",
        letterSpacing: "0.005em",
        color: "rgba(0, 0, 0, 0.8)",
        margin: 0,
      },
    },
  },
}));
