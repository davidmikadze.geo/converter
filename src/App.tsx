import Converter from "./components/converter";
import Container from "@material-ui/core/Container";

function App() {
  return (
    <Container fixed>
      <Converter title="Currency converter" />
    </Container>
  );
}

export default App;
