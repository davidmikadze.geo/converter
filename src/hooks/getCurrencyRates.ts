import useSWR from "swr";
import { fetcher } from "./utils";

export const URL = "https://www.cbr-xml-daily.ru/latest.js";

export function useGetCurrencyRates() {
  const { data, error, isValidating, mutate } = useSWR(URL, fetcher, {
    //refresh rates every minute
    refreshInterval: 60000,
  });

  return {
    data,
    isLoading: isValidating,
    isError: error,
    mutate,
  };
}
