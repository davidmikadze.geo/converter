export const STORAGE_KEY = "c-converter-defaults";
const DEFAULT_STATE = {
  currency: "USD",
  amount: 1,
};

export interface IRatesMap {
  [key: string]: number;
}

export interface ICurrencyFXInfo {
  amount: number;
  currency: string;
}

export interface ICurrencyService {
  store(amount: number, currency: string): void;
  getStored(): ICurrencyFXInfo;
}

export class CurrencyService implements ICurrencyService {
  public getStored(): ICurrencyFXInfo {
    const data = localStorage.getItem(STORAGE_KEY);

    if (data) {
      try {
        return JSON.parse(data);
      } catch (err) {
        console.error(err);
        return DEFAULT_STATE;
      }
    }
    return DEFAULT_STATE;
  }

  public store(amount: number, currency: string): void {
    localStorage.setItem(STORAGE_KEY, JSON.stringify({ amount, currency }));
  }

  static normalizeData({
    rates,
    base,
  }: {
    rates: IRatesMap;
    base: string;
  }): IRatesMap {
    return { ...rates, [base]: 1 };
  }

  static getRateForCCYPair(
    CCYFrom: string,
    CCYTo: string,
    rates: { [key: string]: number }
  ): number {
    return rates[CCYTo] / rates[CCYFrom];
  }
}

export default new CurrencyService();
