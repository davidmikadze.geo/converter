import CurrencyService, {
  STORAGE_KEY,
  CurrencyService as CCYService,
} from "./CurrencyService";

test("stores slected currency and amount to localstorage", () => {
  const selectedCCY = "GBP";
  const selectedAmount = 100;

  CurrencyService.store(selectedAmount, selectedCCY);

  const LSString = localStorage.getItem(STORAGE_KEY);
  const fromLS = LSString && JSON.parse(LSString);

  expect(fromLS.currency).toBe(selectedCCY);
  expect(Number(fromLS.amount)).toBe(selectedAmount);
});

test("gets stored slected currency and amount from localstorage", () => {
  const selectedCCY = "GBP";
  const selectedAmount = 100;

  CurrencyService.store(selectedAmount, selectedCCY);
  const stored = CurrencyService.getStored();

  expect(stored.currency).toBe(selectedCCY);
  expect(Number(stored.amount)).toBe(selectedAmount);
});

test("normalize method transforms data to required shape", () => {
  const data = {
    base: "RUB",
    rates: {
      USD: 3.4,
      GBP: 4.1,
    },
  };

  const newData = CCYService.normalizeData(data);

  expect(newData).toEqual({
    RUB: 1,
    USD: 3.4,
    GBP: 4.1,
  });
});
